<?php

namespace Doggo\Task;

use Doggo\Model\Park;
use GuzzleHttp\Client;
use SilverStripe\Dev\BuildTask;
use SilverStripe\ORM\DB;

class FetchParksTask extends BuildTask
{
    // Palmerston North City Council Park API
    private static $api_url_pncc;
    private static $api_title_pncc;
    
    // Wellington City Council Park API
    private static $api_url_wcc;
    private static $api_title_wcc;

    public function run($request)
    {
        // Fetch Palmerston North City Council Parks
        $this->FetchPNCCParks();

        // Fetch Wellington City Council Parks
        $this->FetchWCCParks();
    }

    private function FetchPNCCParks()
    {
        $client = new Client();

        $response = $client->request(
            'GET',
            $this->config()->get('api_url_pncc'),
            ['User-Agent' => 'Doggo (www.somar.co.nz)']
        );

        if ($response->getStatusCode() !== 200) {
            user_error('Could not access ' . $this->config()->get('api_url_pncc'));
            exit;
        }

        /*
         * Mark existing records as IsToPurge.
         *
         * As we encounter each record in the API source, we unset this.
         * Once done, any still set are deleted.
         */
        $existingParks = Park::get()->filter('Provider', $this->config()->get('api_title_pncc'));
        foreach ($existingParks as $park) {
            $park->IsToPurge = true;
            $park->write();
        }

        $data = json_decode($response->getBody());

        $parks = $data->features;
        foreach ($parks as $park) {

            // Check required fields aren't null
            if ($park->properties->OBJECTID === null ||
                $park->properties->DESCRIPTION === null ||
                $park->properties->RESERVE_NAME == null ||
                $park->geometry === null)
            {
            continue;
            }


            $parkObject = Park::get()->filter([
                'Provider' => $this->config()->get('api_title_pncc'),
                'ProviderCode' => $park->properties->OBJECTID,
            ])->first();
            $status = 'changed';

            if (!$parkObject) {
                $status = 'created';
                $parkObject = Park::create();
                $parkObject->Provider = $this->config()->get('api_title_pncc');
                $parkObject->ProviderCode = $park->properties->OBJECTID;
            }

            if ($park->properties->DESCRIPTION === 'Dog exercise area') {
                $leash = 'Off-leash';
            } elseif ($park->properties->DESCRIPTION === 'Dogs prohibited') {
                continue;
            } else {
                $leash = 'On-leash';
            }

            if ($park->geometry->type === 'MultiPolygon') {
                $geometry = $park->geometry->coordinates[0][0][0];
            }
            else {
                $geometry = $park->geometry->coordinates[0][0];
            }

            $parkObject->update([
                'IsToPurge' => false,
                'Title' => $park->properties->RESERVE_NAME,
                'Latitude' => $geometry[0],
                'Longitude' => $geometry[1],
                'Notes' => $park->properties->DESCRIPTION,
                'GeoJson' => json_encode($park),
                'FeatureOnOffLeash' => $leash,
            ]);

            $parkObject->write();

            DB::alteration_message('[' . $parkObject->ProviderCode . '] ' . $parkObject->Title . ', Managed By ' . $parkObject->Provider, $status);
        }

        $existingParks = Park::get()->filter([
            'Provider' => $this->config()->get('api_title_pncc'),
            'IsToPurge' => true,
        ]);
        foreach ($existingParks as $park) {
            DB::alteration_message('[' . $parkObject->ProviderCode . '] ' . $parkObject->Title . ', Managed By ' . $parkObject->Provider, 'deleted');
            $park->delete();
        }
    }

    private function FetchWCCParks()
    {
        $client = new Client();

        $response = $client->request(
            'GET',
            $this->config()->get('api_url_wcc'),
            ['User-Agent' => 'Doggo (www.somar.co.nz)']
        );

        if ($response->getStatusCode() !== 200) {
            user_error('Could not access ' . $this->config()->get('api_url_wcc'));
            exit;
        }

        /*
         * Mark existing records as IsToPurge.
         *
         * As we encounter each record in the API source, we unset this.
         * Once done, any still set are deleted.
         */
        $existingParks = Park::get()->filter('Provider', $this->config()->get('api_title_wcc'));
        foreach ($existingParks as $park) {
            $park->IsToPurge = true;
            $park->write();
        }

        $data = json_decode($response->getBody());

        $parks = $data->features;
        foreach ($parks as $park) {
            $parkObject = Park::get()->filter([
                'Provider' => $this->config()->get('api_title_wcc'),
                'ProviderCode' => $park->properties->GlobalID,
            ])->first();
            $status = 'changed';

            if (!$parkObject) {
                $status = 'created';
                $parkObject = Park::create();
                $parkObject->Provider = $this->config()->get('api_title_wcc');
                $parkObject->ProviderCode = $park->properties->GlobalID;
            }

            if ($park->properties->On_Off === 'Off leash') {
                $leash = 'Off-leash';
            } elseif ($park->properties->On_Off === 'Prohibited') {
                continue;
            } else {
                $leash = 'On-leash';
            }

            if ($park->geometry->type === 'MultiPolygon') {
                $geometry = $park->geometry->coordinates[0][0][0];
            }
            else {
                $geometry = $park->geometry->coordinates[0][0];
            }

            $parkObject->update([
                'IsToPurge' => false,
                'Title' => $park->properties->name,
                'Latitude' => $geometry[0],
                'Longitude' => $geometry[1],
                'Notes' => $park->properties->Details,
                'GeoJson' => json_encode($park),
                'FeatureOnOffLeash' => $leash,
            ]);

            $parkObject->write();

            DB::alteration_message('[' . $parkObject->ProviderCode . '] ' . $parkObject->Title . ', Managed By ' . $parkObject->Provider, $status);
        }

        $existingParks = Park::get()->filter([
            'Provider' => $this->config()->get('api_title_wcc'),
            'IsToPurge' => true,
        ]);
        foreach ($existingParks as $park) {
            DB::alteration_message('[' . $parkObject->ProviderCode . '] ' . $parkObject->Title . ', Managed By ' . $parkObject->Provider, 'deleted');
            $park->delete();
        }
    }
}
